# FL_workshop



## Creating a virtual environment

- It is recommanded to have anaconda installed ([link](https://www.anaconda.com/download)).
- From the anaconda prompt execute the following command:
```
conda create -n fl_workshop -y python=3.7
```

## Installing the dependencies 

- Clone/download the repository,  extract the dataset_part1.zip and dataset_part2.zip, then inside the anaconda prompt use the following commands:
```
conda activate fl_workshop
cd /path/to/fl_workshop
pip install -r requirements.txt
```

## Centralized training

- Open jupyter lab from the anaconda command prompt:
```
jupyterlab
```
- Open the jupyter notebook named "Centralised-Blood cell images classification for malaria detection.ipynb"
- execute the code cells.

## Federated Learning training

- To launch the server, Open a new anaconda prompt and type the following command:
```
conda activate fl_workshop
cd /path/to/fl_workshop
python server.py

```
- Launch the first client:
```
conda activate fl_workshop
cd /path/to/fl_workshop
python client1.py --dataset_path ./dataset_part1

```
- Launch the second client:
```
conda activate fl_workshop
cd /path/to/fl_workshop
python client2.py --dataset_path ./dataset_part2

```
