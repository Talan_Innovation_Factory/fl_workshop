from typing import Any, Callable, Dict, List, Optional, Tuple

import flwr as fl
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.layers import Convolution2D, MaxPooling2D, Flatten, Dense, BatchNormalization, Dropout
from tensorflow.keras.models import Sequential
from PIL import Image
import os 
import numpy  as np

def main() -> None:
    # Load and compile model for
    # server-side parameter initialization
    model=build_model()
    # Create strategy
    strategy = fl.server.strategy.FedAvg(
        fraction_fit=1.0,
        fraction_eval=1.0,
        min_fit_clients=2,
        min_eval_clients=2,
        min_available_clients=2,
        on_fit_config_fn=fit_config,
        eval_fn=get_eval_fn(model),
        on_evaluate_config_fn=evaluate_config,
        initial_parameters=fl.common.weights_to_parameters(model.get_weights()),

    )

    # Start Flower server for four rounds of federated learning
    fl.server.start_server("localhost:3000",
                            config={"num_rounds": 1}, 
                            strategy=strategy,
                            grpc_max_message_length = 1024*1024*1024,
                            force_final_distributed_eval = True)


def fit_config(rnd: int):
    """Return training configuration dict for each round.
    Keep batch size fixed at 32, perform two rounds of training with 10
    local epoch.
    """
    config = {
        "batch_size": 32,
        "local_epochs": 10,
    }
    return config


def evaluate_config(rnd: int):
    """Return evaluation configuration dict for each round.
    Perform five local evaluation steps on each client (i.e., use five
    batches) during rounds one to three, then increase to ten local
    evaluation steps.
    """
    val_steps = 10
    return {"val_steps": val_steps}

def get_eval_fn(model):
    """Return an evaluation function for server-side evaluation."""

    # Load data and model here to avoid the overhead of doing it in `evaluate` itself

    SIZE=64
    parasitized_images = os.listdir("./dataset_val/Parasitized/")
    uninfected_images = os.listdir('./dataset_val/Uninfected/')
    dataset=[]
    label=[]
    if (len(parasitized_images)>0) and (len(uninfected_images)>0):
        for i, (image_name_parasited,image_name_uninfected) in enumerate(zip(parasitized_images,uninfected_images)):
            if ('.png' in image_name_parasited) and ('.png' in image_name_uninfected):
                image_parasited = Image.open("./dataset_val/Parasitized/" + image_name_parasited)
                image_uninfected = Image.open('./dataset_val/Uninfected/' + image_name_uninfected)
                image_parasited = image_parasited.resize((SIZE, SIZE))
                image_uninfected = image_uninfected.resize((SIZE, SIZE))
                dataset.append(np.array(image_parasited))
                label.append(0)
                dataset.append(np.array(image_uninfected))
                label.append(1)
    x_val= np.array(dataset)
    y_val= np.array(label)
    # The `evaluate` function will be called after every round
    def evaluate(
        weights: fl.common.Weights,
    ) -> Optional[Tuple[float, Dict[str, fl.common.Scalar]]]:
        model.set_weights(weights)  # Update model with the latest parameters
        loss, accuracy = model.evaluate(x_val, y_val)
        return loss, {"accuracy": accuracy}

    return evaluate

def build_model():
    SIZE = 64  # our neural nets expect input data of same format so we will use resize our images to (size x size) 
    model = Sequential()
    model.add(Convolution2D(32, (3, 3), input_shape = (SIZE, SIZE, 3), activation = 'relu', data_format='channels_last'))
    model.add(MaxPooling2D(pool_size = (2, 2), data_format="channels_last"))
    model.add(BatchNormalization(axis = -1))
    model.add(Dropout(0.2))
    model.add(Convolution2D(32, (3, 3), activation = 'relu'))
    model.add(MaxPooling2D(pool_size = (2, 2), data_format="channels_last"))
    model.add(BatchNormalization(axis = -1))
    model.add(Dropout(0.2))
    model.add(Flatten())
    model.add(Dense(activation = 'relu', units=512))
    model.add(BatchNormalization(axis = -1))
    model.add(Dropout(0.2))
    model.add(Dense(activation = 'relu', units=256))
    model.add(BatchNormalization(axis = -1))
    model.add(Dropout(0.2))
    model.add(Dense(activation = 'softmax', units=2))
   # we compile our model 
    model.compile(optimizer = 'adam', loss = 'sparse_categorical_crossentropy', metrics = ['accuracy'])
    return model

if __name__ == "__main__":
    main()